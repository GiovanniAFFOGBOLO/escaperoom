import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessiveComponent } from './lessive.component';

describe('LessiveComponent', () => {
  let component: LessiveComponent;
  let fixture: ComponentFixture<LessiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
