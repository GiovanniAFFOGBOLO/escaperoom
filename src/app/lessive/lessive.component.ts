import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lessive',
  templateUrl: './lessive.component.html',
  styleUrls: ['./lessive.component.scss']
})
export class LessiveComponent implements OnInit {
  lessive: string = '';
  musique: number;

  zik: boolean = false;
  showError1: boolean = false;
  showOK1: boolean = false;
  showError2: boolean = false;
  showOK2: boolean = false;
  nextBtn: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  lessiveCheck() {
    this.showError1 = false;
    this.showOK1 = false;

    if (this.lessive == 'jotaro') {
      this.showOK1 = true;
      this.zik = true;
    } else {
      this.showError1 = true;
      this.zik = false;
    }
  }

  musiqueCheck() {
    if (this.musique == 2009) { // année de mort de michael jackson
      this.showOK2 = true;
      if (this.showOK1 == true) {
        this.nextBtn = true;
      }
    } else {
      this.showError2 = true;
    }
  }

  next() {
    this.router.navigate(['mots']);
  }

}
