import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {
  charade: string = '';
  motCache: string = '';

  pair: boolean = false;
  showError1: boolean = false;
  showOK1: boolean = false;
  showError2: boolean = false;
  showOK2: boolean = false;
  nextBtn: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  charadeCheck() {
    this.showError1 = false;
    
    if (this.charade == 'pair') {
      this.showOK1 = true;
      this.pair = true;
    } else {
      this.showError1 = true;
    }
  }

  motCacheCheck() {
    if (this.motCache == 'sushi') {
      this.showOK2 = true;
      if (this.showOK1 == true) {
        this.nextBtn = true;
      }
    } else {
      this.showError2 = true;
    }
  }

  next() {
    this.router.navigate(['fantome']);
  }


}
