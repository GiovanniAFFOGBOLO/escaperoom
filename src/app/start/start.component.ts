import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  video: boolean = true;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToHome(){
    this.video = false;
    this.router.navigate(['home']);
  }
}
