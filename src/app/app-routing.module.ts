import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GamesComponent } from './games/games.component';
import { HomeComponent } from './home/home.component';
import { StartComponent } from './start/start.component';
import { FantomeComponent } from './fantome/fantome.component';
import { MotsCroisesComponent } from './mots-croises/mots-croises.component';
import { LessiveComponent } from './lessive/lessive.component';

const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'start', component: StartComponent },
  { path: 'home', component: HomeComponent },
  { path: 'games', component: GamesComponent },
  { path: 'fantome', component: FantomeComponent },
  { path: 'lessive', component: LessiveComponent },
  { path: 'mots', component: MotsCroisesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
