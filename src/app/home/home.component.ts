import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  start: any;
  codeBouleBillard: string = '';
  showError: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  checkMotDePasse(){
    console.log(this.codeBouleBillard);
    if(this.codeBouleBillard == 'thebellfelldown'){
      this.router.navigate(['games']);
    } else {
      this.showError = true;
    }
  }

}
