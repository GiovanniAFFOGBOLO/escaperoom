import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { GamesComponent } from './games/games.component';
import { StartComponent } from './start/start.component';
import { FantomeComponent } from './fantome/fantome.component';
import { LessiveComponent } from './lessive/lessive.component';
import { MotsCroisesComponent } from './mots-croises/mots-croises.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GamesComponent,
    StartComponent,
    FantomeComponent,
    LessiveComponent,
    MotsCroisesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
