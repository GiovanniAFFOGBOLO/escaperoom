import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fantome',
  templateUrl: './fantome.component.html',
  styleUrls: ['./fantome.component.scss']
})
export class FantomeComponent implements OnInit {

  message: string = '';
  showError: boolean = false;
  showOK: boolean = false;
  nextBtn: boolean = false;
  constructor(private router: Router) { }


  ngOnInit() {
  }
  messageCheck() {
    this.showOK = false;
    this.showError = false;
    if (this.message == 'manomano') {
      this.showOK = true;
      this.nextBtn = true;
    } else {
      this.showError = true;
    }
  }

  next() {
    this.router.navigate(['lessive']);
  }


}
